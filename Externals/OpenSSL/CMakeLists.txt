# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building OpenSSL if it's not available on the
# build machine.
#

# Stop if the build is not needed:

find_package( OpenSSL 1.1 QUIET )
if( OPENSSL_FOUND )
    # no build needed
    return()
endif()

# The name of the package:
atlas_subdir( OpenSSL )

# Build the OpenSSL library ourselves, and point the XRootD build at it.
message(STATUS "BUILDING: OpenSSL 1.1.1")
set( _opensslBuildDir
        "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/OpenSSLBuild" )
# todo: make rpath work with the openSSL build - so that paths to build area don't end up in libs
ExternalProject_Add( OpenSSL
        PREFIX "${CMAKE_BINARY_DIR}"
        INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
        URL "https://cern.ch/lcgpackages/tarFiles/sources/openssl-1.1.1.tar.gz"
        URL_MD5 "7079eb017429e0ffb9efb42bf80ccb21"
        DOWNLOAD_EXTRACT_TIMESTAMP FALSE
        CONFIGURE_COMMAND <SOURCE_DIR>/config --prefix=${_opensslBuildDir}
        UPDATE_COMMAND ""
        UPDATE_DISCONNECTED 1
        LOG_CONFIGURE 1 LOG_DOWNLOAD 1 LOG_BUILD 1 LOG_INSTALL 1 )
ExternalProject_Add_Step( OpenSSL buildinstall
        COMMAND ${CMAKE_COMMAND} -E copy_directory
        ${_opensslBuildDir}/ <INSTALL_DIR>
        COMMENT "Installing OpenSSL into the build area"
        DEPENDEES install )
install( DIRECTORY "${_opensslBuildDir}/"
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
add_dependencies( Package_OpenSSL OpenSSL )

add_library( OpenSSL::SSL SHARED IMPORTED GLOBAL )
set_target_properties( OpenSSL::SSL PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${_opensslBuildDir}/include"
        IMPORTED_LOCATION "${_opensslBuildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}ssl${CMAKE_SHARED_LIBRARY_SUFFIX}")
add_library( OpenSSL::Crypto SHARED IMPORTED GLOBAL )
set_target_properties( OpenSSL::Crypto PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${_opensslBuildDir}/include"
        IMPORTED_LOCATION "${_opensslBuildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}crypto${CMAKE_SHARED_LIBRARY_SUFFIX}")
unset( _opensslBuildDir )
