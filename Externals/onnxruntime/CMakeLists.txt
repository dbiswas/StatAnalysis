# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building ONNX Runtime as part of the offline / analysis
# release.
#

# This package needs CMake 3.18 at least.
cmake_minimum_required( VERSION 3.18 )

# Set the name of the package:
atlas_subdir( onnxruntime )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Find the optional externals.
option( ATLAS_ONNXRUNTIME_USE_CUDA
   "Use the CUDA capabilities of ONNX Runtime, if possible" FALSE )
mark_as_advanced( ATLAS_ONNXRUNTIME_USE_CUDA )
if( ATLAS_ONNXRUNTIME_USE_CUDA )
   find_package( CUDAToolkit )
   find_package( cuDNN )
endif()

# Declare where to get ONNXRuntime from.
if( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" )
   set( ATLAS_ONNXRUNTIME_SOURCE_DEFAULT
      "URL;http://cern.ch/atlas-software-dist-eos/externals/onnxruntime/onnxruntime-linux-x64-1.15.1.tgz;https://github.com/microsoft/onnxruntime/releases/download/v1.15.1/onnxruntime-linux-x64-1.15.1.tgz;URL_MD5;05b1aa5b682beaeb2ae3d9915b9553f6" )
   if( CUDAToolkit_FOUND AND cuDNN_FOUND )
      set( ATLAS_ONNXRUNTIME_SOURCE_DEFAULT
         "URL;http://cern.ch/atlas-software-dist-eos/externals/onnxruntime/onnxruntime-linux-x64-gpu-1.15.1.tgz;https://github.com/microsoft/onnxruntime/releases/download/v1.15.1/onnxruntime-linux-x64-gpu-1.15.1.tgz;URL_MD5;8d2f5ee9f449bdecb10a45715fe74c53" )
   endif()
elseif( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "aarch64" )
   set( ATLAS_ONNXRUNTIME_SOURCE_DEFAULT
      "URL;http://cern.ch/atlas-software-dist-eos/externals/onnxruntime/onnxruntime-linux-aarch64-1.15.1.tgz;https://github.com/microsoft/onnxruntime/releases/download/v1.15.1/onnxruntime-linux-aarch64-1.15.1.tgz;URL_MD5;ebec0b185c9bec94fde884a97b144c04" )
endif()
set( ATLAS_ONNXRUNTIME_SOURCE "${ATLAS_ONNXRUNTIME_SOURCE_DEFAULT}"
   CACHE STRING "The source for ONNXRuntime" )
mark_as_advanced( ATLAS_ONNXRUNTIME_SOURCE )

# Stop here if there's no valid binary for the current platform.
if( "${ATLAS_ONNXRUNTIME_SOURCE}" STREQUAL "" )
   message( WARNING "No ONNXRuntime binary available for the current platform" )
   return()
endif()

# Decide whether / how to patch the ONNXRuntime sources.
set( ATLAS_ONNXRUNTIME_PATCH ""
     CACHE STRING "Patch command for ONNXRuntime" )
set( ATLAS_ONNXRUNTIME_PATCH "" CACHE STRING "Patch command for ONNXRuntime" )
set( ATLAS_ONNXRUNTIME_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of ONNXRuntime (2024.03.08.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_ONNXRUNTIME_PATCH
   ATLAS_ONNXRUNTIME_FORCEDOWNLOAD_MESSAGE )

# Build onnxruntime.
ExternalProject_Add( onnxruntime
   PREFIX "${CMAKE_BINARY_DIR}"
   SOURCE_DIR "${CMAKE_BINARY_DIR}/src/onnxruntime"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_ONNXRUNTIME_SOURCE}
   ${ATLAS_ONNXRUNTIME_PATCH}
   CONFIGURE_COMMAND cmake -E echo "No configuration for ONNXRuntime"
   BUILD_COMMAND cmake -E rename "<SOURCE_DIR>/LICENSE"
                                 "<SOURCE_DIR>/LICENSE.onnxruntime"
   COMMAND cmake -E rename "<SOURCE_DIR>/ThirdPartyNotices.txt"
                           "<SOURCE_DIR>/ThirdPartyNotices.onnxruntime"
   COMMAND cmake -E remove "<SOURCE_DIR>/GIT_COMMIT_ID"
                           "<SOURCE_DIR>/VERSION_NUMBER"
                           "<SOURCE_DIR>/Privacy.md"
                           "<SOURCE_DIR>/README.md"
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory "<SOURCE_DIR>"
                                                      "<INSTALL_DIR>" )
add_dependencies( Package_onnxruntime onnxruntime )

# Install onnxruntime.
install( DIRECTORY "${CMAKE_BINARY_DIR}/src/onnxruntime/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
install( FILES "cmake/Findonnxruntime.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
