# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# GitLab Continuous Integration configuration for the repository.
#
# It checks that:
#  - The project can be built from scratch, and packaged into an RPM file.
#  - A Docker image can be created with the help of the RPM file, and uploaded
#    into gitlab-registry.cern.ch.

# this snippet prevents pipelines starting for branches that have an MR open, so as to avoid duplications
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

# "Global" build stages.
stages:
  - build
  - collect_versions
  - docker
  - test
  - docker_base_image

# Set the behaviour of the CI build.
variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: recursive

# Each job needs to start with setting up the build environment of the base
# image.
before_script:
  - export MAKEFLAGS="-j4" # reduce resource requirement on Gitlab runners
  - echo $MAKEFLAGS
  - export PATH=/opt/cmake/3.27.5/Linux-x86_64/bin:/opt/lcg/ccache/4.7.4-3759c/x86_64-centos9-gcc11-opt/bin:$PATH
  - export MAIN_CACHE_URL="https://gitlab.cern.ch/api/v4/projects/122672/jobs/artifacts/main/raw/cache.zip?job=build"
  - export CACHE_URL="https://gitlab.cern.ch/api/v4/projects/122672/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/cache.zip?job=build"
  - mkdir -p ${CI_PROJECT_DIR}/cache
  - export CCACHE_DIR=${CI_PROJECT_DIR}/cache

# Configuration for the CMake build job. This job rebuilds the full repo and saves
# the build directory as an artifac
build:
  stage: build
  image: gitlab-registry.cern.ch/atlas/statanalysis:base-image-alma
  script:
    - source /opt/lcg/gcc/13.1.0*/x86_64-*/setup.sh
    - export CMAKE_CXX_COMPILER_LAUNCHER=ccache;
    - ccache -M 3G
    # - >
    #   if [ "404" != `curl -I $CACHE_URL | head -n 1|cut -d$' ' -f2` ]; then
    #     curl --output cache.zip "$CACHE_URL";
    #     unzip -n -q cache.zip; rm cache.zip;
    #   elif [ "404" != `curl -I $MAIN_CACHE_URL | head -n 1|cut -d$' ' -f2` ]; then
    #     curl --output cache.zip "$MAIN_CACHE_URL";
    #     unzip -n -q cache.zip; rm cache.zip;
    #   fi
    - mkdir -p ci_build
    - cmake -DCMAKE_BUILD_TYPE=Release -DCCACHE=ON -DCERN_GITLAB_PREFIX="https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch" -S . -B ci_build/
    - cd ci_build/
    - cmake --build .
    - cpack -G RPM
    - cd ..
    - zip -q -r -9 ./cache.zip cache
    - du -hs cache.zip
  artifacts:
    paths:
      - ci_build/*.rpm
      - ci_build/CMakeCache.txt
      - ci_build/**/*-stamp/*.log
      - cache.zip
    expire_in: never
    when: always
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $BUILD_BASE_IMAGE == "true"
      when: never
    - when: on_success

# Setup for building a Docker image from the repository's compiled code.
# This step also pushes the docker image if it is on the main repo 
# and not in a fork
docker:
    stage: docker
    image: 
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
    script:
        # Prepare Kaniko configuration file
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        # Build and push the image from the Dockerfile at the root of the project.
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile $DESTINATION_FLAG
        # Print the full registry path of the pushed image
        - echo "Image pushed successfully with ${DESTINATION_FLAG}"
    rules:
      - if: (($CI_PIPELINE_SOURCE == "schedule"  && $BUILD_BASE_IMAGE == "true"))
        when: never
      - if: $CI_PROJECT_NAMESPACE == "atlas"
        variables:
          DESTINATION_FLAG: --destination gitlab-registry.cern.ch/atlas/statanalysis:${CI_COMMIT_REF_SLUG}
      - if: $CI_PROJECT_NAMESPACE != "atlas"
        when: never # don't run docker image step in forks
      - when: on_success

# Test by building and executing the testpackage
test:
  stage: docker
  image: gitlab-registry.cern.ch/atlas/statanalysis:base-image-alma
  script:
    - source /opt/lcg/gcc/13.1.0*/x86_64-*/setup.sh
    - export gcc_version="$(gcc -dumpfullversion)"; export gcc_version="${gcc_version%%.*}"
    - sudo rpm -i --replacepkgs --prefix="$(pwd)" ci_build/*.rpm # Install statanalysis in current directory
    - source StatAnalysis/*/InstallArea/*/setup.sh # Source statanalysis
    - mkdir TestProject/build
    - cd TestProject/build
    - cmake .. -Dbuild_test=true # Build testproject
    - make
    - ctest --output-on-failure --output-junit ./../testRes.xml # Actually execute tests embedded in testproject
    - ls ./../
  allow_failure: true # IMPORTANT: Should remove this line once tests are fixed (can handle missing components)
  artifacts:
    when: always
    reports:
      junit: TestProject/testRes.xml
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"  && $BUILD_BASE_IMAGE == "true"
      when: never
    - when: on_success

# This script only builds a base image with yum installations
docker_base_image:
    stage: docker_base_image
    image: 
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
    variables:
        DESTINATION: gitlab-registry.cern.ch/atlas/statanalysis:${CI_COMMIT_REF_SLUG}
        DOCKERFILE_PATH: ${CI_PROJECT_DIR}/docker/baseImage/Dockerfile
    script:
        # Prepare Kaniko configuration file
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        # Build and push the image from the Dockerfile at the root of the project.
        - /kaniko/executor --context $CI_PROJECT_DIR --destination ${DESTINATION} --dockerfile ${DOCKERFILE_PATH}
        # Print the full registry path of the pushed image
        - echo "Image pushed successfully to ${DESTINATION}"
    rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"  && $BUILD_BASE_IMAGE == "true"
