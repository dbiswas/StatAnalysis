# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Set up the project.
cmake_minimum_required( VERSION 3.6 )
project( WorkDir VERSION 1.0.0 LANGUAGES C CXX )

# Look for an existing StatAnalysis release.
find_package( StatAnalysis REQUIRED )

# Set up CTest.
atlas_ctest_setup()

# Set up the project with all of its packages.
atlas_project( USE StatAnalysis ${StatAnalysis_VERSION}
   PROJECT_ROOT ${CMAKE_SOURCE_DIR}/../ )

# Generate the environment setup for the externals. No replacements need to be
# done to it, so it can be used for both the build and the installed release.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Package up the release using CPack.
atlas_cpack_setup()
