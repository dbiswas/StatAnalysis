#!/usr/bin/env python

import sys
import csv

def compare(filename_1, filename_2, etol=2e-2):

    with open(filename_1) as file_1:
        with open(filename_2) as file_2:
            file_1_reader = csv.reader(file_1, delimiter=",")
            file_2_reader = csv.reader(file_2, delimiter=",")

            for line_1, line_2 in zip(file_1_reader, file_2_reader):
                diff_min = abs(float(line_1[1]) - float(line_2[1]))
                diff_max = abs(float(line_1[2]) - float(line_2[2]))
                if diff_min > etol or diff_max > etol:
                    print("Difference exceeds error tolerance of " + str(etol) + \
                    " in the lines: \n", ' '.join(line_1), "|", ' '.join(line_2))
                    return 1

    return 0

if __name__ == "__main__":

    if compare(sys.argv[1], sys.argv[2]):
        sys.exit(1)
    else:
        sys.exit(0)
    
